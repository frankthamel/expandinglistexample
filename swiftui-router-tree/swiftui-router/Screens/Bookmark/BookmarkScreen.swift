//
//  BookmarkScreen.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-02.
//

import SwiftUI

protocol BookmarkScreenRouter: AnyObject {
}

struct BookmarkScreen: View {
    @State var router: BookmarkScreenRouter?
    
    @ObservedObject var viewModel: BookmarkViewModel
    
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct BookmarkScreenPreviews: PreviewProvider {
    static var previews: some View {
        BookmarkScreen(viewModel: BookmarkViewModel(services: MockServices()))
    }
}
