//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation
import CoreData
import DI
import Combine

private struct CoreDataStorageProviderKey: InjectionKey {
    static var currentValue: CoreDataStorageProvider = CoreDataStorageProvider()
}

public extension InjectedValues {
    var coreDataStorageProvider: CoreDataStorageProvider {
        get { Self[CoreDataStorageProviderKey.self] }
        set { Self[CoreDataStorageProviderKey.self] = newValue }
    }
}

public class CoreDataStorageProvider {
    public let persistentContainer: NSPersistentContainer
    
    init() {
        persistentContainer = NSPersistentCloudKitContainer(name: "DatabaseModel")
        persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Core Data Store failed to load with error: \(error.localizedDescription)")
            }
        }
        
        persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
        persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    }
}

extension CoreDataStorageProvider {
    func saveViewContext() throws {
        try persistentContainer.viewContext.save()
    }
}

class FetchedObjectList<Object: NSManagedObject>: NSObject, NSFetchedResultsControllerDelegate {
    
    let fetchedResultsController: NSFetchedResultsController<Object>
    
    private let onContentChange = PassthroughSubject<(), Never>()
    private let onObjectChange = PassthroughSubject<Object, Never>()
    
    init(
        fetchRequest: NSFetchRequest<Object>,
        managedObjectContext: NSManagedObjectContext)
    {
        fetchedResultsController =
        NSFetchedResultsController(fetchRequest: fetchRequest,
                                   managedObjectContext: managedObjectContext,
                                   sectionNameKeyPath: nil,
                                   cacheName: nil)
        super.init()
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch {
            NSLog("Error fetching objects: \(error)")
        }
    }
    
    var objects: [Object] {
        fetchedResultsController.fetchedObjects ?? []
    }
    
    var contentDidChange: AnyPublisher<(), Never> {
        onContentChange.eraseToAnyPublisher()
    }
    
    var objectDidChange: AnyPublisher<Object, Never> {
        onObjectChange.eraseToAnyPublisher()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        onContentChange.send()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        onObjectChange.send(anObject as! Object)
    }
}


