//
//  SideBarViewModel.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-04.
//

import Foundation
import Combine
import SwiftUI

class SideBarViewModel: BaseViewModel<Services>, ObservableObject {
    @Published var model: [SideBarMenuModel] = menuItems
    @Published var selection: Set<Bool> = []
    
}

let tag1 = SideBarMenuModel(title: "Tag 1", route: .accounts, icon: "tag.fill", isExpanded: true, titleLevel: .titleLevel4, color: .red, children: nil)
let tag2 = SideBarMenuModel(title: "Tag 2", route: .accounts, icon: "tag.fill", isExpanded: true, titleLevel: .titleLevel4, color: .green, children: nil)
let tag3 = SideBarMenuModel(title: "Tag 3", route: .accounts, icon: "tag.fill", isExpanded: true, titleLevel: .titleLevel4, color: .blue, children: nil)
let tag4 = SideBarMenuModel(title: "Tag 4", route: .accounts, icon: "tag.fill", isExpanded: true, titleLevel: .titleLevel4, color: .purple, children: nil)

let accounntsMenuItem = SideBarMenuModel(title: "Accounts", route: .accounts, icon: "scribble.variable", isExpanded: false, titleLevel: .titleLevel3, color: .red, children: nil)
let bookmarksMenuItem = SideBarMenuModel(title: "Bookmarks", route: .bookmarks,icon: "book", isExpanded: false, titleLevel: .titleLevel3, color: .green, children: [tag1, tag2, tag3, tag4])
let homeMenuItem = SideBarMenuModel(title: "Home", route: .home,icon: "house.fill", isExpanded: false, titleLevel: .titleLevel2, color: .blue, children: nil)
let loginMenuItem = SideBarMenuModel(title: "Login", route: .login,icon: "person.fill.badge.plus", isExpanded: false, titleLevel: .titleLevel2, color: .pink, children: nil)
let profileMenuItem = SideBarMenuModel(title: "Profile", route: .profile,icon: "person.fill", isExpanded: false, titleLevel: .titleLevel2, color: .pink, children: [accounntsMenuItem, bookmarksMenuItem])

let title1 = SideBarMenuModel(title: "Favourites", route: .title, icon: "heart", isExpanded: true, titleLevel: .titleLevel1, color: .red, nonDeletable: true, children: [homeMenuItem, loginMenuItem])
let title2 = SideBarMenuModel(title: "App Home", route: .title, icon: "folder", isExpanded: true, titleLevel: .titleLevel1, color: .blue, children: [profileMenuItem])
let title3 = SideBarMenuModel(title: "Shortcuts", route: .bookmarks, icon: "link", isExpanded: false, titleLevel: .titleLevel1, color: .blue, nonDeletable: true, children: [profileMenuItem])
let title4 = SideBarMenuModel(title: "Actions", route: .bookmarks, icon: "character.book.closed", isExpanded: true, titleLevel: .titleLevel1, color: .orange, nonDeletable: true, children: [profileMenuItem])

var menuItems = [title3, title1, title4, title2]
var rootDataModel = SideBarMenuModel(title: "Bookmarks", route: .home, icon: "house", isExpanded: true, titleLevel: .root, color: .red, children: menuItems)
