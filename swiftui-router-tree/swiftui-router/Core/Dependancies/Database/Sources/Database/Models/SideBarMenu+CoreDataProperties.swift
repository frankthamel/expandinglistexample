//
//  SideBarMenu+CoreDataProperties.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-11.
//
//

import Foundation
import CoreData


extension SideBarMenu {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SideBarMenu> {
        return NSFetchRequest<SideBarMenu>(entityName: "SideBarMenu")
    }

    @NSManaged public var color: Int16
    @NSManaged public var icon: String?
    @NSManaged public var isExpanded: Bool
    @NSManaged public var menuID: String?
    @NSManaged public var nonDeletable: Bool
    @NSManaged public var parent: String?
    @NSManaged public var title: String?
    @NSManaged public var titleLevel: String?

}

extension SideBarMenu : Identifiable {

}
