//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation
import Networking
import Database
import DI
import Combine

public protocol SideBarMenuRepository {
    func saveSideBarMenu(menu: SideBarMenuRepo) throws
    func deleteSideBarMenu(menu: SideBarMenuRepo) throws
    func getSideBarMenu() -> AnyPublisher<[SideBarMenuRepo], Never>
}

public struct SideBarMenuRepositoryImpl: SideBarMenuRepository {
    
    @Injected(\.networkProvider) var networkProvider: NetworkProviding
    @Injected(\.databaseProvider) var databaseProvider: DatabaseProviding
    
    public func saveSideBarMenu(menu: SideBarMenuRepo) throws {
        let sideBarMenu = menu.toSidebarMenu()
        try databaseProvider.saveSideBarMenu(menu: sideBarMenu)
    }
    
    public func getSideBarMenu() -> AnyPublisher<[SideBarMenuRepo], Never> {
        let items = try databaseProvider.fetchSideBarMenuItems()
            .map { items in
                items.map { SideBarMenuRepo(from: $0) }
            }
            .eraseToAnyPublisher()
        
        return items
    }
    
    public func deleteSideBarMenu(menu: SideBarMenuRepo) throws {
        try databaseProvider.deleteSideBarMenu(menuID: menu.menuID)
    }
}

public struct MockSideBarMenuRepositoryImpl: SideBarMenuRepository {
    public func saveSideBarMenu(menu: SideBarMenuRepo) throws {
        print("saveSideBarMenu MockSideBarMenuRepositoryImpl")
    }
    
    public func getSideBarMenu() -> AnyPublisher<[SideBarMenuRepo], Never> {
//        let menuItem = SideBarMenuRepo(menuID: UUID().uuidString,
//                                       title: "Bookmark",
//                                       icon: "book",
//                                       isExpanded: true,
//                                       titleLevel: "2",
//                                       color: 2,
//                                       nonDeletable: false,
//                                       parent: UUID().uuidString)
        let data = PassthroughSubject<[SideBarMenuRepo], Never>()
        return data.eraseToAnyPublisher()
    }
    
    public func deleteSideBarMenu(menu: SideBarMenuRepo) throws {
        
    }
}
