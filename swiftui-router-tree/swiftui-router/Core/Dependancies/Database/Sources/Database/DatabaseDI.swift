//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation
import DI

private struct DatabaseProviderKey: InjectionKey {
    // Set default data provider to CoreData data provider
    static var currentValue: DatabaseProviding = CoreDataDatabaseProvider()
}

public extension InjectedValues {
    var databaseProvider: DatabaseProviding {
        get { Self[DatabaseProviderKey.self] }
        set { Self[DatabaseProviderKey.self] = newValue }
    }
}
