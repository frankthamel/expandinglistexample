//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-02.
//

import Foundation

// MARK: NetworkProvider

public protocol NetworkProviding {
    init()
    func requestData()
}

public struct NetworkProvider: NetworkProviding {
    
    public init() {}
    
    public func requestData() {
        print("Data requested using the `NetworkProvider`")
    }
}

public struct MockedNetworkProvider: NetworkProviding {
    
    public init() {}
    
    public func requestData() {
        print("Data requested using the `MockedNetworkProvider`")
    }
}
