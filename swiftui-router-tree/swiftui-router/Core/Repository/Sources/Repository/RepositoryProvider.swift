//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation

// MARK: RepositoryProvider

public protocol RepositoryProviding {
    init()
    var sideBarMenuRepository: SideBarMenuRepository { get set }
}

public struct RepositoryProvider: RepositoryProviding {
    
    public init() {}
    
    public var sideBarMenuRepository: SideBarMenuRepository = SideBarMenuRepositoryImpl()

}

public struct MockedRepositoryProvider: RepositoryProviding {
    
    public init() {}
    
    public var sideBarMenuRepository: SideBarMenuRepository = MockSideBarMenuRepositoryImpl()
}
