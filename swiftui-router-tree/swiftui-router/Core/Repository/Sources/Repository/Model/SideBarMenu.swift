//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation
import Database
import DI

public struct SideBarMenuRepo {
    
    @Injected(\.coreDataStorageProvider) var coreDataStorageProvider: CoreDataStorageProvider

    public var menuID: String
    public var title: String
    public var icon: String
    public var isExpanded: Bool
    public var titleLevel: String
    public var color: Int
    public var nonDeletable: Bool
    public var parent: String?
    
    public init(menuID: String,
                  title: String,
                  icon: String,
                  isExpanded: Bool,
                  titleLevel: String,
                  color: Int,
                  nonDeletable: Bool,
                  parent: String? = nil) {
        self.menuID = menuID
        self.title = title
        self.icon = icon
        self.isExpanded = isExpanded
        self.titleLevel = titleLevel
        self.color = color
        self.nonDeletable = nonDeletable
        self.parent = parent
    }
    
    init(from sideBarMenu: SideBarMenu) {
        self.menuID = sideBarMenu.menuID ?? ""
        self.title = sideBarMenu.title ?? ""
        self.icon = sideBarMenu.icon ?? ""
        self.isExpanded = sideBarMenu.isExpanded
        self.titleLevel = sideBarMenu.titleLevel ?? ""
        self.color = Int(sideBarMenu.color)
        self.nonDeletable = sideBarMenu.nonDeletable
        self.parent = sideBarMenu.parent
    }
    
}

extension SideBarMenuRepo {
    
    // Creating core data model
    func toSidebarMenu() -> SideBarMenu {
        let item = SideBarMenu(context: coreDataStorageProvider.persistentContainer.viewContext)
    
        item.title = self.title
        item.menuID = self.menuID
        item.icon = self.icon
        item.isExpanded = self.isExpanded
        item.titleLevel = self.titleLevel
        item.color = Int16(self.color)
        item.nonDeletable = self.nonDeletable
        item.parent = self.parent
        
//        item.objectWillChange.sink { <#Void#> in
//            <#code#>
//        }
        
        return item
    }
}

