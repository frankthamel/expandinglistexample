//
//  SideBarRouter.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-04.
//

import Foundation
import SwiftUI

class SideBarRouter: ObservableObject {
    // MARK: - Published vars
    
    // MARK: - Private vars
    
    // MARK: - Internal vars
    var services: Services
        
    // MARK: - Initialization

    init(services: Services) {
        self.services = services
    }
    
    // MARK: - Methods
    
    @ViewBuilder func contentView() -> some View {
        SideBarScreen(router: self, viewModel: SideBarViewModel(services: self.services))
    }
    
    @ViewBuilder func bookmarksView() -> BookmarksScreen {
        BookmarksScreen(router: self, viewModel: BookmarksViewModel(services: services))
    }
    
    @ViewBuilder func account() -> some View {
        AccountRouterView(router: AccountRouter(services: self.services))
    }
}

struct SideBarRouterView: View {
    @StateObject var router: SideBarRouter
        
    var body: some View {
        NavigationView {
            self.router.contentView()
        }
    }
}

extension SideBarRouter: SideBarScreenRouter {
    func bookmarksScreen() -> BookmarksScreen {
        self.bookmarksView()
    }
    
    func destinationForRoute(_ route: Route) -> AnyView {
        switch route {
        case .home:
            return AnyView(bookmarksView())
        case .accounts:
            return AnyView(account())
        case .bookmarks:
            return AnyView(bookmarksView())
        case .login:
            return AnyView(bookmarksView())
        case .profile:
            return AnyView(bookmarksView())
        case .title:
            return AnyView(EmptyView())
        }
    }
}


extension SideBarRouter: BookmarksScreenRouter {}
