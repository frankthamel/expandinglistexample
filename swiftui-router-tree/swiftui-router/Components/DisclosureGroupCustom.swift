//
//  DisclosureGroup.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-06.
//

import SwiftUI

public protocol Expandable {
    var isExpanded: Bool {  get set }
    var titleLevel: TitleLevel { get set }
}

public enum TitleLevel {
    case root
    case titleLevel1
    case titleLevel2
    case titleLevel3
    case titleLevel4
}

struct NodeOutlineGroup<Node, R: View, A: View, B: View, C: View, D: View>: View where Node: Hashable, Node: Identifiable, Node: CustomStringConvertible, Node: Expandable {
    @State var node: Node
    let childKeyPath: KeyPath<Node, [Node]?>
    let rootLevel: (Node) -> R
    let titleLevel1: (Node) -> A
    let titleLevel2: (Node) -> B
    let titleLevel3: (Node) -> C
    let titleLevel4: (Node) -> D
    
    var body: some View {
        if node[keyPath: childKeyPath] != nil {
            DisclosureGroup(
                isExpanded: $node.isExpanded,
                content: {
                    if node.isExpanded {
                        ForEach(node[keyPath: childKeyPath]!) { childNode in
                            NodeOutlineGroup(node: childNode,
                                             childKeyPath: childKeyPath,
                                             rootLevel: rootLevel,
                                             titleLevel1: titleLevel1,
                                             titleLevel2: titleLevel2,
                                             titleLevel3: titleLevel3,
                                             titleLevel4: titleLevel4
                            )
                        }
                    }
                },
                label: {
                    titleFor(level: node.titleLevel)
                })
        } else {
            titleFor(level: node.titleLevel)
        }
    }
    
    @ViewBuilder
    func titleFor(level: TitleLevel) -> some View {
        switch level {
        case .root:
            rootLevel(node)
        case .titleLevel1:
            titleLevel1(node)
        case .titleLevel2:
            titleLevel2(node)
        case .titleLevel3:
            titleLevel3(node)
        case .titleLevel4:
            titleLevel4(node)
        }
    }
}

struct RootMenuTitleStyle: View {
    let node: SideBarMenuModel
    var body: some View {
        HStack{
            Text(node.title)
                .font(.title2)
            Spacer()
        }
    }
}

let ICON_MIN_WIDTH: CGFloat = 30

struct Level1MenuTitleStyle: View {
    let node: SideBarMenuModel
    var body: some View {
        
        HStack{
            Image(systemName: node.icon)
                .foregroundColor(node.color)
                .minWidth(ICON_MIN_WIDTH)
            Text(node.title)
                .font(.headline)
            Spacer()
        }
        .contextMenu {
            ContextMenuContent(child: node)
        }
    }
}


struct Level2MenuTitleStyle: View {
    let node: SideBarMenuModel
    var body: some View {
        
        HStack{
            Image(systemName: node.icon)
                .foregroundColor(node.color)
                .minWidth(ICON_MIN_WIDTH)
            Text(node.title)
                .font(.subheadline)
            Spacer()
        }
        .contextMenu {
            ContextMenuContent(child: node)
        }
    }
}

struct Level3MenuTitleStyle: View {
    let node: SideBarMenuModel
    var body: some View {
        
        HStack{
            Image(systemName: node.icon)
                .foregroundColor(node.color)
                .minWidth(ICON_MIN_WIDTH)
            Text(node.title)
                .font(.footnote)
            Spacer()
        }
        .contextMenu {
            ContextMenuContent(child: node)
        }
    }
}

struct Level4MenuTitleStyle: View {
    let node: SideBarMenuModel
    var body: some View {
        
        HStack{
            Image(systemName: node.icon)
                .foregroundColor(node.color)
                .minWidth(ICON_MIN_WIDTH)
            Text(node.title)
                .font(.footnote, weight: .light)
            Spacer()
        }
        .contextMenu {
            ContextMenuContent(child: node)
        }
    }
}
