//
//  EditableList.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-08.
//

import SwiftUI

struct EditableList: View {
    @State private var users = ["Paul", "Taylor", "Adele"]
    
    
    var body: some View {
        NavigationView {
            
            VStack {
                ZStack {
                    Rectangle().fill(.red)
                    TextField("", text: $users[0])
                        .padding()
                    
                    List {
                        ForEach(users, id: \.self) { user in
                            Text(user)
                        }
                        .onMove(perform: move)
                        .onDelete { indexSet in
                            for index in indexSet {
                                users.remove(at: index)
                            }
                        }
                        
                        Section {
                            ForEach(users, id: \.self) { user in
                                Text(user)
                            }
                            .onMove(perform: move)
                            .onDelete { indexSet in
                                for index in indexSet {
                                    users.remove(at: index)
                                }
                            }
                            
                            Section {
                                ForEach(users, id: \.self) { user in
                                    HStack {
                                        Text(user)
                                            .padding(.horizontal, 10)
                                            .padding(.vertical, 10)
                                            
                                        Spacer()
                                        Rectangle()
                                            .fill(Color.red)
                                            .frame(width: 50, height: 50)
                                            .contextMenu {
                                                Text("123")
                                            }
                                        
                                    }
                                    .background(Color.clear)
                                    .cornerRadius(5)
                                    
                                    .onDrag { NSItemProvider(item: nil, typeIdentifier: "123") }
                                    
                                    
                                    
                                }
                                .onMove(perform: move)
                                .onDelete { indexSet in
                                    for index in indexSet {
                                        users.remove(at: index)
                                    }
                                }
                                
                                Section {
                                    ForEach(users, id: \.self) { user in
                                        Text(user)
                                            .frame(minWidth:0, maxWidth:.infinity, minHeight:50)
                                            .border(Color.black)
                                            .background(Color.red)
                                            .onDrag { NSItemProvider(item: nil, typeIdentifier: "123") }
                                            .contextMenu {
                                                Text("123")
                                            }
                                        
                                    }
                                    .onMove(perform: move)
                                    .onDelete { indexSet in
                                        for index in indexSet {
                                            users.remove(at: index)
                                        }
                                    }
                                } header: {
                                    VStack {
                                        HStack {
                                            Text("Section Header")
                                            Spacer()
                                        }
                                    }
                                    
                                }
                            } header: {
                                VStack {
                                    HStack {
                                        Text("Section Header")
                                        Spacer()
                                    }
                                }
                                
                            }
                            
                        } header: {
                            VStack {
                                HStack {
                                    Text("Section Header")
                                    Spacer()
                                }
                                Divider()
                            }
                        }
                        
                    }
                    .listStyle(SidebarListStyle())
                    .background(.green)
                    .toolbar {
                        EditButton()
                    }
                }
                
            }
            
        }
    }
    
    func move(from source: IndexSet, to destination: Int) {
        users.move(fromOffsets: source, toOffset: destination)
    }
}

struct EditableList_Previews: PreviewProvider {
    static var previews: some View {
        EditableList()
    }
}
