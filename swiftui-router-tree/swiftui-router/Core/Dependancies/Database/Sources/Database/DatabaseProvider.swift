//
//  File.swift
//  
//
//  Created by Frank Thamel on 2022-01-11.
//

import Foundation
import DI
import CoreData
import Combine

// MARK: NetworkProvider

public protocol DatabaseProviding {
    init()
    func requestData()
    
    // MARK: SideBarMenu
    func saveSideBarMenu(menu: SideBarMenu) throws
    func fetchSideBarMenuItems() -> AnyPublisher<[SideBarMenu], Never>
    func deleteSideBarMenu(menuID: String) throws
}

public struct CoreDataDatabaseProvider: DatabaseProviding {
    
    @Injected(\.coreDataStorageProvider) var coreDataStorageProvider: CoreDataStorageProvider
    
    public init() {
        // storageProvider = CoreDataStorageProvider()
    }
        
    public func requestData() {
        print("Data requested using the `CoreDataDatabaseProvider`")
    }
    
    public func saveSideBarMenu(menu: SideBarMenu) throws {
        try menu.managedObjectContext?.save()
    }
    
    public func fetchSideBarMenuItems() -> AnyPublisher<[SideBarMenu], Never> {
        let fetchRequest: NSFetchRequest<SideBarMenu> = SideBarMenu.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \SideBarMenu.title, ascending: true)]
//        let item = try coreDataStorageProvider.persistentContainer.viewContext.fetch(fetchRequest)
//        return item
        
        let fetchedObjectList = FetchedObjectList(fetchRequest: fetchRequest,
                                                     managedObjectContext: coreDataStorageProvider.persistentContainer.viewContext)
        
        let fetchedObjectPublisher = fetchedObjectList.contentDidChange
            .prepend(())
            .map {
                fetchedObjectList.objects
            }
            .eraseToAnyPublisher()
        
        return fetchedObjectPublisher
    }
    
    public func deleteSideBarMenu(menuID: String) throws {
        
        let fetchRequest: NSFetchRequest<SideBarMenu> = SideBarMenu.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "menuID == %@", menuID)
        if let fetchedResult = try coreDataStorageProvider.persistentContainer.viewContext
            .fetch(fetchRequest)
            .first {
            coreDataStorageProvider.persistentContainer.viewContext.delete(fetchedResult)
        }
        
        try coreDataStorageProvider.saveViewContext()
    }
    
}

public struct MockedDatabaseProvider: DatabaseProviding {

    public init() {}
    
    public func requestData() {
        print("Data requested using the `MockedDatabaseProvider`")
    }
    
    public func saveSideBarMenu(menu: SideBarMenu) {
        print("Sidebar menu: \(menu.title ?? "") saved.")
    }
    
    public func fetchSideBarMenuItems() -> AnyPublisher<[SideBarMenu], Never> {
        let data = PassthroughSubject<[SideBarMenu], Never>()
        return data.eraseToAnyPublisher()
    }
    
    public func deleteSideBarMenu(menuID: String) throws{
        
    }
}
