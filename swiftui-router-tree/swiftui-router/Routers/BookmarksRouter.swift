//
//  BookmarksRouter.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-02.
//

import Foundation
import SwiftUI

class BookmarksRouter: ObservableObject {
    // MARK: - Published vars
    
    // MARK: - Private vars
    
    // MARK: - Internal vars
    var services: Services
        
    // MARK: - Initialization

    init(services: Services) {
        self.services = services
    }
    
    // MARK: - Methods
    
    @ViewBuilder func bookmarks() -> some View {
        BookmarksScreen(router: self, viewModel: BookmarksViewModel(services: services))
    }
}

struct BookmarksRouterView: View {
    @StateObject var router: BookmarksRouter
        
    var body: some View {
        NavigationView {
            self.router.bookmarks()
        }
        self.router.bookmarks()
    }
}

extension BookmarksRouter: BookmarksScreenRouter {
    
}
