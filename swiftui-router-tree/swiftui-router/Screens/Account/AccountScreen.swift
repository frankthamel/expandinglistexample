//
//  AccountScreen.swift
//  RouterTest
//
//  Created by David Gary Wood on 1/05/21.
//

import SwiftUI
import Repository

protocol AccountScreenRouter: AnyObject {
    func accountScreenProfileScreen() -> ProfileScreen
}

struct AccountScreen: View {
    @State var router: AccountScreenRouter    
    @StateObject var viewModel: AccountScreenViewModel
    
    var body: some View {
        VStack {
//            List {
//                NavigationLink(destination: self.router.accountScreenProfileScreen()) {
//                    Text("User Profile")
//                }
//            }.listStyle(PlainListStyle())
            
            List {
                ForEach(viewModel.menuItems, id: \.menuID) { item in
                    Label(item.title, systemImage: item.icon)
                }
                
            }
            
            HStack {
                Button {
                    self.viewModel.addData()
                } label: {
                    Text("Add")
                }
                .padding()
                .background(.blue)
                .foregroundColor(.white)
                .cornerRadius(8)
                
                Button {
                    self.viewModel.delete()
                } label: {
                    Text("Delete")
                }
                .padding()
                .background(.red)
                .foregroundColor(.white)
                .cornerRadius(8)
                
                Button {
                    self.viewModel.addData()
                } label: {
                    Text("Update")
                }
                .padding()
                .background(.green)
                .foregroundColor(.white)
                .cornerRadius(8)
            }
            
            
            Spacer()
            
            Button {
                self.viewModel.logout()
            } label: {
                Text("Log out")
            }
        }
        .padding()
        .onAppear {
            viewModel.loadData()
        }
    }
}

struct AccountScreenPreviews: PreviewProvider {
    static var previews: some View {
        let vm = AccountScreenViewModel(services: MockServices())
        vm.repositoryProvider = MockedRepositoryProvider()
        
        return AccountScreen(router: AccountRouter(services: MockServices()), viewModel: vm)
    }
}
