//
//  RouterTestApp.swift
//  RouterTest
//
//  Created by David Gary Wood on 24/04/21.
//

import SwiftUI
import Core

@main
struct RouterTestApp: App {
    
    init() {
        // InjectedValues[\.networkProvider] = MockedNetworkProvider()
        // set app theme
        // based on system set initial theme (dark / light)
        UITableView.appearance().separatorColor = .clear
        UITableView.appearance().separatorStyle = .none
        UITableViewCell.appearance().backgroundColor = .clear
        UITableViewCell.appearance().tintColor = UIColor(named: "tableTint")
        UITableView.appearance().backgroundColor = UIColor(named: "primaryBackground")
        
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.setBackIndicatorImage(UIImage(systemName: "arrow.turn.up.left"),
                                               transitionMaskImage: UIImage(systemName: "arrow.turn.up.left"))
        navBarAppearance.shadowColor = .clear
        navBarAppearance.shadowImage = nil
        navBarAppearance.shadowColor = .none
        
        UINavigationBar.appearance().standardAppearance = navBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
        UINavigationBar.appearance().compactAppearance = navBarAppearance
        UINavigationBar.appearance().backgroundColor = UIColor(named: "primaryBackground")
        UINavigationBar.appearance().shadowImage = nil
        UINavigationBar.appearance().standardAppearance.configureWithTransparentBackground()
        UINavigationBar.appearance().scrollEdgeAppearance?.backgroundColor = UIColor(named: "primaryBackground")
        UINavigationBar.appearance().tintColor = UIColor(named: "backButton")
        
        //UIWindow.appearance().tintColor = UIColor(named: "backButton")
    }
    
    var body: some Scene {
        WindowGroup {
            AppRouterView(router: AppRouter(services: AppServices()))
            //DisclosureGroupCustom()
            //DragAndDropView()
            //EditableList()
        }
    }
}
