//
//  BookmarksScreen.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-02.
//

import SwiftUI
import CoreUI
import SwiftUIX


protocol BookmarksScreenRouter: AnyObject {
}

struct BookmarksScreen: View {
    let gridItems: [GridItem] = [GridItem(spacing: 10, alignment: .topLeading)]
    @State var router: BookmarksScreenRouter?
    
    @ObservedObject var viewModel: BookmarksViewModel
    
    @ViewBuilder
    var body: some View {
        content
            .onAppear {
                viewModel.loadBookmarks()
            }
    }
    
    @ViewBuilder
    var content: some View {
        if UIDevice().model == "iPad" {
            ScrollView(.horizontal) {
                LazyHGrid(rows: gridItems, alignment: .top, spacing: 0) {
                    ForEach(viewModel.bookmarks) { bookmark in
                        BookmarksContent(bookmark: bookmark)
                    }
                }
                .padding()
                
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarTitle("All Bookmarks")
        } else {
            PaginationView(axis: .horizontal) {
                ForEach(viewModel.bookmarks) { bookmark in
                    BookmarksContent(bookmark: bookmark)
                }
            }
            //.currentPageIndex($...)
            .pageIndicatorAlignment(.center)
            .pageIndicatorTintColor(.systemGray)
            .currentPageIndicatorTintColor(.green)
            .padding()
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarTitle("All Bookmarks")
        }
    }
}

struct BookmarksScreenPreviews: PreviewProvider {
    static var previews: some View {
        BookmarksScreen(viewModel: BookmarksViewModel(services: MockServices()))
    }
}

struct TagChipView: View {
    
    let title: String
    let color: Color
    
    var body: some View {
        Label(title, systemImage: "tag.fill")
            .font(.footnote)
            .foregroundColor(.white)
            .padding(.horizontal, 6)
            .padding(.vertical, 4)
            .background(color)
            .cornerRadius(6)
    }
}

struct BookmarksContent: View {
    
    let bookmark: Bookmark
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(bookmark.title)")
                .font(.title)
                .bold()
                .fontWeight(.regular)
                .frame(minWidth: 300, alignment: .leading)
                .multilineTextAlignment(.leading)
                //.padding(.top, 20)
            
            ScrollView {
                LazyVStack(spacing: 5) {
                    ForEach(0..<20) { i in
                        HStack(alignment: .firstTextBaseline) {
                            Image(systemName: "checkmark.square")
                            VStack(alignment: .leading) {
                                Text("Swift Structured Concurrency Swift Structured Concurrency")
                                    .frame(maxWidth: 300, alignment: .leading)
                                    .lineLimit(3)
                                Text("2021-12-31, 12:00")
                                    .font(.footnote)
                                    .foregroundColor(.secondary)
                                
                                HStack {
                                    TagChipView(title: "Swift", color: .orange)
                                    TagChipView(title: "Concurrency", color: .pink)
                                    //TagChipView(title: "SwiftUI", color: .green)
                                }
                            }
                        }
                        .padding()
                        .background(Rectangle().foregroundColor(.black))
                        .cornerRadius(15)
                        .contextMenu {
                            Button("Red") {
                                print("Red \(i)")
                            }
                            
                            Button("Green") {
                                print("Green \(i)")
                            }
                        }
                    }
                }
                .cornerRadius(15)
            }
        }
        .padding(.leading, 10)
    }
}
