//
//  Route.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-04.
//

import Foundation

enum Route {
    case home
    case accounts
    case bookmarks
    case login
    case profile
    case title
}
