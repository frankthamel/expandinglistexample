//
//  SideBarContextMenuContent.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-08.
//

import Foundation
import SwiftUI

struct ContextMenuContent: View {
    
    let child: SideBarMenuModel
    
    var body: some View {
        contextMenuContent(child: child)
    }
    
    @ViewBuilder
    func contextMenuContent(child: SideBarMenuModel) -> some View {
        
        let menuTags = ContextMenuModel().contextMenuItems(for: child.titleLevel)
        
        if child.nonDeletable {
            let actionModel = ContextMenuModel.ActionModel(action: .modifyFields, icon: "slider.horizontal.3")
            Button {
                print("\(child.title) \(actionModel.action)")
            } label: {
                Label(actionModel.action.value, systemImage: actionModel.icon)
            }
    
        } else {
            ForEach(menuTags) { menuTag in
                VStack {
                    contextMenuRow(child: child, menuTag: menuTag)
                }
            }
        }
    }
    
    @ViewBuilder
    func contextMenuRow(child: SideBarMenuModel, menuTag: ContextMenuModel.ActionModel) -> some View {
        if menuTag.action == .delete {
            if #available(iOS 15.0, *) {
                Button(role: .destructive) {
                    print("Delete")
                } label: {
                    Label("Delete", systemImage: menuTag.icon)
                }
                
            } else {
                Button(action: {
                    print("Delete")
                }) {
                    Label("Delete", systemImage: menuTag.icon)
                        .foregroundColor(Color.red)
                }
            }
        } else {
            Button(action: {
                print("\(child.title) -> \(menuTag.action)")
            }) {
                Label(menuTag.action.value, systemImage: menuTag.icon)
            }
        }
    }
}
