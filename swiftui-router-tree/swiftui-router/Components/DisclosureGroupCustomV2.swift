//
//  DisclosureGroupCustomV2.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-09.
//

import SwiftUI

public struct HierarchyList<Data, RowContent>: View where Data: RandomAccessCollection, Data.Element: Identifiable, RowContent: View, Data.Element: Expandable {
  private let recursiveView: RecursiveView<Data, RowContent>

  public init(data: Data, children: KeyPath<Data.Element, Data?>, rowContent: @escaping (Data.Element) -> RowContent) {
    self.recursiveView = RecursiveView(data: data, children: children, rowContent: rowContent)
  }

  public var body: some View {
      
      if UIDevice.current.userInterfaceIdiom == .phone {
          List {
              recursiveView//.hideRowSeparator()
          }
          .listStyle(PlainListStyle())
          .listRowBackground(Color("primaryBackground"))
      } else {
          List {
              recursiveView
          }
          .listStyle(SidebarListStyle())
          .listRowBackground(Color("primaryBackground"))
      }
      
  }
}

private struct RecursiveView<Data, RowContent>: View where Data: RandomAccessCollection, Data.Element: Identifiable, RowContent: View, Data.Element: Expandable {
  let data: Data
  let children: KeyPath<Data.Element, Data?>
  let rowContent: (Data.Element) -> RowContent

  var body: some View {
    ForEach(data) { child in
      if let subChildren = child[keyPath: children] {
          FSDisclosureGroup(isExpanded: child.isExpanded, content: {
          RecursiveView(data: subChildren, children: children, rowContent: rowContent)
        }, label: {
            rowContent(child)
        })
      } else {
        rowContent(child)
      }
    }
  }
}

struct FSDisclosureGroup<Label, Content>: View where Label: View, Content: View {
  @State var isExpanded: Bool = true
  var content: () -> Content
  var label: () -> Label

  @ViewBuilder
  var body: some View {
    DisclosureGroup(isExpanded: $isExpanded, content: content, label: label)
  }
}
