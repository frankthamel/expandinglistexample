//
//  ContextMenuModel.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-08.
//

import Foundation

struct ContextMenuModel {
    enum Actions: String, Identifiable {
        
        var id: String {
            return self.rawValue.trimmingCharacters(in: .whitespaces)
        }
        
        var value: String {
            return  self.rawValue
        }
        
        case edit = "Edit"
        case delete = "Delete"
        case move = "Move"
        case lock = "Lock" // implement later - lock using faceId or fingerprint
        case sync = "Sync"
        case modifyFields = "Modify Fields"
        case add = "Add"
        case addBookmark = "Add Bookmark"
        case hideStatusOnBoard = "Hide Status On Board"
        case showStatusOnBoard = "Show Status On Board"
    }
    
    struct ActionModel: Identifiable {
        var id = UUID()
        let action: Actions
        let icon: String
    }
    
    // Level 1 - Folder
    // Level 2 - Project
    // Level 3 - Tags, Priority, Status, Action
    // Level 4 - Fields
    
    let level1ContextMenu: [ActionModel] = [ ActionModel(action: .edit, icon: "pencil"),
                                             ActionModel(action: .delete, icon: "trash")]
    
    let level2ContextMenu: [ActionModel] = [ActionModel(action: .edit, icon: "pencil"),
                                            ActionModel(action: .move, icon: "folder"),
                                            ActionModel(action: .lock, icon: "faceid"),
                                            ActionModel(action: .sync, icon: "arrow.triangle.2.circlepath"),
                                            ActionModel(action: .delete, icon: "trash")]
    
    let level3ContextMenu: [ActionModel] = [ActionModel(action: .edit, icon: "pencil"),
                                            ActionModel(action: .modifyFields, icon: "slider.horizontal.3"),
                                            ActionModel(action: .add, icon: "plus")]
    
    let level4ContextMenu: [ActionModel] = [ActionModel(action: .addBookmark, icon: "book"),
                                            ActionModel(action: .edit, icon: "pencil"),
                                            ActionModel(action: .delete, icon: "trash")]
    
    func contextMenuItems(for level: TitleLevel) -> [ActionModel] {
        switch level {
        case .root:
            return []
        case .titleLevel1:
            return level1ContextMenu
        case .titleLevel2:
            return level2ContextMenu
        case .titleLevel3:
            return level3ContextMenu
        case .titleLevel4:
            return level4ContextMenu
        }
    }
}
