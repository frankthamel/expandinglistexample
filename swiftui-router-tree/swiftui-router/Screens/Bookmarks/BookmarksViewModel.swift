//
//  BookmarksViewModel.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-02.
//

import Foundation
import Combine

class BookmarksViewModel: BaseViewModel<Services>, ObservableObject {

    @Published var bookmarks: [Bookmark] = Bookmark.preview
    //[Bookmark(title: "Apple", url: "https://www.apple.com")]
    
    func loadBookmarks() {
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 5) { [weak self] in
            DispatchQueue.main.async {
                self?.bookmarks = Bookmark.preview
            }
        }
    }
    
}


struct Bookmark: Identifiable, Hashable {
    let id = UUID()
    let title: String
    let url: String
    
    static let preview: [Bookmark] = [
        Bookmark(title: "Apple", url: "https://www.apple.com"),
        Bookmark(title: "Medium", url: "https://medium.com"),
        Bookmark(title: "Cocoacasts", url: "https://www.apple.com"),
        Bookmark(title: "NSScreenCasts", url: "https://www.apple.com"),
        Bookmark(title: "Facebook", url: "https://www.facebook.com"),
        Bookmark(title: "Google", url: "https://www.google.com"),
        Bookmark(title: "Twitter", url: "https://www.twitter.com"),
    ]
}
