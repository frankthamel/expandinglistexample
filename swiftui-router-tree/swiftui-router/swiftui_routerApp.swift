//
//  swiftui_routerApp.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-04.
//

import SwiftUI

@main
struct swiftui_routerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
