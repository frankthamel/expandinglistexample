//
//  AccountScreenViewModel.swift
//  RouterTest
//
//  Created by David Gary Wood on 1/05/21.
//

import Foundation
import Combine
import Repository
import DI

class AccountScreenViewModel: BaseViewModel<Services>, ObservableObject {
    
    //@Injected(\.networkProvider) var networkProvider: NetworkProviding
    //@Injected(\.databaseProvider) var databaseProvider: DatabaseProviding
    @Injected(\.repositoryProvider) var repositoryProvider: RepositoryProviding
    
    @Published var menuItems: [SideBarMenuRepo] = []
    
    private var cancellables = Set<AnyCancellable>()
    
    override init(services: Services) {
        super.init(services: services)
        Logger.print("init:\(#file)")
    }
    
    deinit {
        Logger.print("deinit:\(#file)")
    }
    
    func logout() {
        self.services.loginManager.logOut()
    }
    
    func addData() {
        do {
            let menuItem = SideBarMenuRepo(menuID: UUID().uuidString,
                                           title: "Bookmark",
                                           icon: "book",
                                           isExpanded: true,
                                           titleLevel: "2",
                                           color: 2,
                                           nonDeletable: false,
                                           parent: UUID().uuidString)
            try repositoryProvider.sideBarMenuRepository.saveSideBarMenu(menu: menuItem)
        } catch {
            print("Cannot save saveSideBarMenu \(error.localizedDescription)")
        }
    }
    
    func loadData() {
        
        repositoryProvider.sideBarMenuRepository.getSideBarMenu().sink {[weak self] items in
            self?.menuItems = items
        }
        .store(in: &cancellables)
        
    }
    
    func update(menuItem: SideBarMenuRepo) {
        
    }
    
    func delete() {
        if let itemToDelete = menuItems.first {
            do {
                try repositoryProvider.sideBarMenuRepository.deleteSideBarMenu(menu: itemToDelete)
            } catch {
                print("Cannot delete SideBarMenu \(error.localizedDescription)")
            }
        }
    }
    
}
