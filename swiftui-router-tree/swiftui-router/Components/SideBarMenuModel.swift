//
//  SideBarMenuModel.swift
//  swiftui-router
//
//  Created by Frank Thamel on 2022-01-08.
//

import Foundation
import SwiftUI


struct SideBarMenuModel: Identifiable, Hashable, CustomStringConvertible, Expandable {

    let id = UUID()
    var title: String
    let route: Route
    let icon: String
    var isExpanded: Bool = false
    var titleLevel: TitleLevel
    var color: Color
    var nonDeletable: Bool = false
    
    var children: [SideBarMenuModel]?
    
    var description: String {
        return title
    }
}

// sortOrder
// contextMenuType


