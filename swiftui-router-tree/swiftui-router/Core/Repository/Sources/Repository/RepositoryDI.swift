import Foundation
import DI

private struct RepositoryProviderKey: InjectionKey {
    static var currentValue: RepositoryProviding = RepositoryProvider()
}

public extension InjectedValues {
    var repositoryProvider: RepositoryProviding {
        get { Self[RepositoryProviderKey.self] }
        set { Self[RepositoryProviderKey.self] = newValue }
    }
}
