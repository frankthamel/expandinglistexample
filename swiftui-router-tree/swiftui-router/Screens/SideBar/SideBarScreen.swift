//
//  SideBarScreen.swift
//  Router
//
//  Created by Frank Thamel on 2022-01-04.
//

import SwiftUI
import CoreUI
import SwiftUIX
import UniformTypeIdentifiers

protocol SideBarScreenRouter: AnyObject {
    func bookmarksScreen() -> BookmarksScreen
    func destinationForRoute(_ route: Route) -> AnyView
}

struct SideBarScreen: View {
    @State var router: SideBarScreenRouter?
    
    @ObservedObject var viewModel: SideBarViewModel
    
    @State var searchText: String = ""
    @State var isEditing: Bool = false
    
    @State var items = ["Apple", "Dell", "Samsung", "Lenovo", "HP", "Apple", "Dell", "Samsung", "Lenovo", "HP", "Apple", "Dell", "Samsung", "Lenovo", "HP"]
    @State var draggedItem : SideBarMenuModel?
    @State var selection: Int?
    
    var body: some View {
        
        ZStack {
            VStack {
                SearchBar("Search...", text: $searchText, isEditing: $isEditing)
                                   .showsCancelButton(isEditing)
                                   .onCancel { print("Canceled!") }
                                   .padding(.horizontal, 10)
                               .padding(.bottom, 0)
                               .background(Color("primaryBackground"))
                
                HierarchyList(data: viewModel.model, children: \.children, rowContent: { menuNavigation(node: $0) })
                    .background(.clear)
                
                Button(action: {
                    withAnimation {
                        menuItems.remove(at: 0)
                        //menuItems[1].title = "Title Changed"
                        viewModel.model = menuItems
                        
                    }
                    
                    print("**** \(menuItems.count)")
                    print("***** \(viewModel.selection)")
                }) {
                    Text("Edit")
                }
            }
            .padding(.top, 10)
            
            
            VStack {
                if searchText.count > 0 {
                    if UIDevice.current.userInterfaceIdiom == .phone {
                        List(items.filter({ searchText.isEmpty ? true : $0.contains(searchText.lowercased()) || $0.contains(searchText) }), id: \.self) { item in
                                    Text(item)
                                    
                                }
                            .listStyle(PlainListStyle())
                            .padding(.horizontal, 5)
                    } else {
                        List(items.filter({ searchText.isEmpty ? true : $0.contains(searchText.lowercased()) || $0.contains(searchText) }), id: \.self) { item in
                                    Text(item)
                                    
                                }
                            .listStyle(SidebarListStyle())
                            .padding(.horizontal, 5)
                    }
                }
                
                Spacer()
                //}
            }
            
            .background(Color("primaryBackground"))
            .padding(.top, 70)
            
            
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarTitle("Home")
        .background(Color("primaryBackground"))
        .edgesIgnoringSafeArea([.bottom])
        
        
        router?.bookmarksScreen()
    }
    
    @ViewBuilder
    func menuNavigation(node: SideBarMenuModel) -> some View {
        NavigationLink(tag: node.hashValue,
                       selection: $selection) {
            router?.destinationForRoute(node.route)
        } label: {
            menuStyle(for: node)
        }
        .opacity(0.0)
        .overlay(
            HStack {
                menuStyle(for: node)
                Spacer()
            }
        )
        .onAppear {
            if self.selection != nil {
                self.selection = nil
            }
        }
    }
    
    @ViewBuilder
    func menuStyle(for node: SideBarMenuModel) -> some View {
        switch node.titleLevel {
        case .root:
            RootMenuTitleStyle(node: node)
        case .titleLevel1:
            Level1MenuTitleStyle(node: node)
        case .titleLevel2:
            Level2MenuTitleStyle(node: node)
        case .titleLevel3:
            Level3MenuTitleStyle(node: node)
        case .titleLevel4:
            Level4MenuTitleStyle(node: node)
        }
    }
}




// Not used
struct HideRowSeparatorModifier: ViewModifier {

  static let defaultListRowHeight: CGFloat = 44

  var insets: EdgeInsets
  var background: Color

  init(insets: EdgeInsets, background: Color) {
    self.insets = insets

    var alpha: CGFloat = 0
    UIColor(background).getWhite(nil, alpha: &alpha)
    assert(alpha == 1, "Setting background to a non-opaque color will result in separators remaining visible.")
    self.background = background
  }

  func body(content: Content) -> some View {
    content
      .padding(insets)
      .frame(
        minWidth: 0, maxWidth: .infinity,
        minHeight: Self.defaultListRowHeight,
        alignment: .leading
      )
      .listRowInsets(EdgeInsets())
      .background(background)
  }
}

public extension EdgeInsets {

  static let defaultListRowInsets = Self(top: 0, leading: 16, bottom: 0, trailing: 16)
}

public extension View {

func hideRowSeparator(
    insets: EdgeInsets = .defaultListRowInsets,
    background: Color = Color("primaryBackground")
  ) -> some View {
    modifier(HideRowSeparatorModifier(
      insets: insets,
      background: background
    ))
  }
}
